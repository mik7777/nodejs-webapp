module.exports = function(app, database) {
  try {
    app.get('/', function(req, res) {
      require('../controllers/indexController.js')(app, database, req, res);
    });

    app.get('/about', function(req, res) {
      res.render('./about.html.twig', {page: 'about'});
    });

    app.post('/game/:id/:operation', function(req, res) {
      require('../controllers/playController.js')(app, database, req, res);
    });

    app.post('/game/:id/:operation/:step', function(req, res) {
      require('../controllers/playController.js')(app, database, req, res);
    });

    app.post('/game/:id/:operation/:step/question/:question', function(req, res) {
      require('../controllers/playController.js')(app, database, req, res);
    });

    app.get('/admin/:operation', function(req, res) {
      require('../controllers/adminController.js')(app, database, req, res);
    });

    app.get('/admin/:operation/:id', function(req, res) {
      require('../controllers/adminController.js')(app, database, req, res);
    });

    app.post('/admin/:operation/:id', function(req, res) {
      require('../controllers/adminController.js')(app, database, req, res);
    });

    app.get('/score/:operation', function(req, res) {
      require('../controllers/scoreController.js')(app, database, req, res);
    });

    app.get('/score/:operation/:id', function(req, res) {
      require('../controllers/scoreController.js')(app, database, req, res);
    });

    app.all('*', function(req, res) {
      require('../controllers/indexController.js')(app, database, req, res);
    });
  } catch (e) {
    console.log('Error, route was not processed.');
    //console.log(e);
  }
};