(function() {
  'use strict';
  try {
    var express = require('express'), // Load express framework
        mongoClient = require('mongodb').MongoClient, // Load mongoDb client
        bodyParser = require('body-parser'),
        config = require('./config/app'), // Load database configuration file
        app = express(), // Initialize express framework
        port = 8000, // Set used port
        twig = require('twig'); // Load twig template engine

    // Reads a form's input and stores it as a javascript object accessible through req.body
    app.use(bodyParser.urlencoded({
      extended: true
    }));

    // Set the path for public files like img, css, js
    app.use(express.static(__dirname + '/public'));

    // Establishing connection with mongodb
    mongoClient.connect(config.url, function(err, database) {
      if (err) {
        // Log connection errors
        return console.log(err);
      }

      // Load routes
      require('./routes/routes')(app, database);
      // Creating an HTTP server
      app.listen(port, function() {
        console.log('We are live on ' + port);
      });
    });
  } catch (e) {
    console.log('Error, cannot run app.');
    //console.log(e);
  }
}());