module.exports = function(app, database, req, res) {
  try {
    // Performed operation, can be start, step, save
    var operation = req.params.operation,
      id = req.params.id,
      step = (typeof req.params.step == 'undefined' || isNaN(req.params.step)) ? '1' : req.params.step.toString(),
      question = req.params.question;

    if (operation == 'start') {
      if (!req.body.name) {
        res.render('./index.html.twig', {
          page: 'home',
          operation: operation,
          message: 'Please type your name!',
        });
      } else {
        var game = {
          userName: req.body.name,
          startDate: new Date(),
          endDate: null,
        }
        // Insert new started game data
        database.collection('game').insert(game, function(err, result) {
          if (err) {
            res.send('Error, cannot start new game.');
          } else {
            res.redirect(308, '/game/' + result.insertedIds[0] + '/step/1');
          }
        });
      }
    } else if (operation == 'step') {
      // If no request body redirect to home page
      if (Object.keys(req.body).length === 0) {
        res.render('./index.html.twig', {
          page: 'home',
          operation: '',
          message: 'You aborted last game, please start new one.',
        });
      } else {
        var condition = [{
            $match: {
              level: step
            }
          }, {
            $sample: {
              size: 1
            }
          }],
          message = '';
        if (question) {
          var mongo = require('mongodb'),
            condition = {
              $match: {
                _id: mongo.ObjectId(question)
              }
            };
          message = 'Error, please select correct answer.';
        }
        database.collection('question').aggregate(condition, function(err, result) {
          // User passed final step
          if (step > 10) {
            // Select all result that corespond to that game and join to result the question
            database.collection('result').aggregate([{
              $match: {
                gameId: id
              }
            }, {
              $lookup: {
                from: 'question',
                localField: 'questionId',
                foreignField: '_id',
                as: 'data'
              }
            }]).toArray(function(err, result) {
              if (err || !result.length) {
                res.send('Error, cannot calculate totals. Start <a href="/">new game</a>?');
              } else {
                // Calculate total number of steps and correct answers
                var total = 0,
                  correct = 0;
                for (var i = 0; i < result.length; i++) {
                  if (result[i].data[0]) {
                    total++;
                    if (result[i].result.every(function(e) {
                        return result[i].data[0].correct_answer[e] == '1'
                      })) {
                      correct++;
                    }
                  }
                }
                var mongo = require('mongodb');
                // Update game, set score and end date
                database.collection('game').update({
                  _id: new mongo.ObjectId(id),
                  endDate: null,
                }, {
                  $set: {
                    endDate: new Date(),
                    score: correct + '/' + total,
                  }
                }, function(err) {
                  if (err) {
                    res.send('Error, the game was not updated. Go <a href="/">back</a>.');
                  } else {
                    res.redirect('/score/view/' + id);
                  }
                });
              }
            });
          } else if (err || !result.length) {
            res.send('Error, cannot find question.');
          } else {
            // Render result page with score
            res.render('./play.html.twig', {
              page: 'home',
              operation: 'question',
              question: result[0],
              step: step,
              gameId: id.toString(),
              message: message,
            });
          }
        });
      }
    } else if (operation == 'save') {
      if (!req.body.result) {
        // User not select answer, remain on the same page
        res.redirect(308, '/game/' + id + '/step/' + step + '/question/' + req.body._id);
      } else if (!id) {
        res.send('Error, missing game id.');
      } else {
        var mongo = require('mongodb');
        // Load game data by id and that is not ended
        database.collection('game').find({
          _id: mongo.ObjectId(id),
          endDate: null,
        }).toArray(function(err, result) {
          if (err || !result.length) {
            res.send('Error, cannot find game.');
          } else {
            var data = {
              gameId: id.toString(),
              questionId: mongo.ObjectId(req.body._id),
              result: req.body.result.constructor === Array ? req.body.result : [req.body.result], // Transform to array if need
              step: step,
              date: new Date(),
            };
            // Save in database selected by user answer
            database.collection('result').insert(data, function(err) {
              if (err) {
                res.send('Error, the results was not saved.');
              } else {
                res.redirect(308, '/game/' + id + '/step/' + (parseInt(step) + 1));
              }
            });
          }
        });
      }
    } else {
      res.send('404');
    }
  } catch (e) {
    console.log('Error, page not rendered.');
    //console.log(e);
  }
};