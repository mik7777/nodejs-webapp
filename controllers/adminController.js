module.exports = function(app, database, req, res) {
  try {
    var mongo = require('mongodb'),
      id = req.params.id,
      operation = req.params.operation;
    if (operation == 'view') {
      database.collection('question').find({}).toArray(function(err, result) {
        if (err || !result.length) {
          res.send('Error, questions was not found. You can <a href="/admin/add">add</a> one.');
        } else {
          res.render('./admin.html.twig', {
            page: 'admin',
            section: 'view',
            operation: operation,
            data: result,
            message: '',
          });
        }
      });
    } else if (operation == 'add') {
      // Render add form
      res.render('./admin.html.twig', {
        page: 'admin',
        section: 'add',
        operation: operation,
        data: {
          _id: 'new',
          question: '',
          level: 0,
          answer: ['', '', '', ''],
          correct_answer: [0, 0, 0, 0],
        },
        message: '',
      });
    } else if (operation == 'edit') {
      // Find question by id
      database.collection('question').find({
        _id: new mongo.ObjectId(id)
      }).toArray(function(err, result) {
        if (err || !result.length) {
          res.send('Error cannot load question. Go <a href="/admin/view">back</a>.');
        } else {
          // Render edit form
          res.render('./admin.html.twig', {
            page: 'admin',
            section: 'edit',
            operation: operation,
            data: result[0],
            message: '',
          });
        }
      });
    } else if (operation == 'save') {
      // Check if user type answers
      var noAswers = req.body.answer.every(function(answer) {
          return answer === ''
        }),
        // Check if correct answer is indicated
        noResponses = req.body.correct_answer.every(function(response) {
          return response === '0'
        }),
        question = {
          question: req.body.question,
          level: req.body.level,
          answer: req.body.answer,
          correct_answer: req.body.correct_answer,
          created: new Date(),
          updated: null,
        };
      if (!question.question || noAswers || noResponses) {
        res.send('Error, please go <a href="/admin/add">back</a> type question, answer and select level.');
      } else if (id == 'new') {
        // Insert new question in bd if id is new
        database.collection('question').insert(question, function(err) {
          if (err) {
            res.send('Error, the question was not added. Go <a href="/admin/add">back</a>.');
          } else {
            res.send('The question was added successfully. Go to <a href="/admin/view">questions list</a>.');
          }
        });
      } else {
        question.changed = new Date();
        // Update question
        database.collection('question').update({
          _id: new mongo.ObjectId(id)
        }, {
          $set: question
        }, function(err) {
          if (err) {
            res.send('Error, the question was not updated. Go <a href="/admin/edit/' + id + '">back</a>.');
          } else {
            res.send('The question was updated successfully. Go to <a href="/admin/view">questions list</a>.');
          }
        });
      }
    } else if (operation == 'remove') {
      // Delete question by id
      database.collection('question').deleteOne({
        _id: new mongo.ObjectId(id)
      }, function(err) {
        if (err) {
          res.send('Error, the question was not deleted. Go to <a href="/admin/view">questions list</a>.');
        } else {
          res.send('The question was deleted. Go to <a href="/admin/view">questions list</a>.');
        }
      });
    } else {
      res.send('404');
    }
  } catch (e) {
    console.log('Error, page not rendered.');
    //console.log(e);
  }
};