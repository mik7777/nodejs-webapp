module.exports = function(app, database, req, res) {
  try {
    // Render home page
    res.render('./index.html.twig', {
      operation: '',
      page: 'home',
      message: '',
    });
  } catch (e) {
    console.log('Error, page not rendered.');
    //console.log(e);
  }
};