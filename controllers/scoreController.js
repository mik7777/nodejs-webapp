module.exports = function(app, database, req, res) {
  try {
    var operation = req.params.operation,
      game = req.params.id;

    if (operation == 'view' && game) {
      // Select user answer for one game by game id
      database.collection('result').aggregate([{
        $match: {
          gameId: game
        }
      }, {
        $lookup: {
          from: 'question',
          localField: 'questionId',
          foreignField: '_id',
          as: 'data'
        }
      }], function(err, result) {
        if (err || !result.length) {
          res.send('Error, cannot find result. Start <a href="/">new game</a>?');
        } else {
          var object = {};
          for (var i = 0; i < result.length; i++) {
            if (result[i].data[0]) {
              object[i] = {
                question: result[i].data[0].question, // Data for question row
                // Determine if answer is correct
                isCorrect: result[i].result.every(function(e) {
                  return result[i].data[0].correct_answer[e] == '1'
                }),
                // Get all corect answers
                correct_answer: result[i].data[0].answer.filter(function(e, j) {
                  return result[i].data[0].correct_answer[j] == '1'
                }),
                // get all posible answers
                answer: result[i].result.map(function(e) {
                  return result[i].data[0].answer[e]
                }),
              }
            } else {
              res.send('Error, cannot load all information. Go to <a href="/">main page</a>.');
            }
          }
          // Render page with user's result for one game 
          res.render('./score.html.twig', {
            page: 'score',
            operation: operation,
            results: object,
          });
        }
      });
    } else if (operation == 'list') {
      // Load all games
      database.collection('game').find({}).toArray(function(err, result) {
        if (err || !result.length) {
          res.send('No game history was found. Start new <a href="/">game</a>.');
        } else {
          // Render page with all played games
          res.render('./score.html.twig', {
            page: 'score',
            operation: operation,
            data: result,
            message: '',
          });
        }
      });
    } else if (operation == 'remove') {
      var mongo = require('mongodb');
      // Delete game record
      database.collection('game').deleteOne({
        _id: new mongo.ObjectId(game)
      }, function(err) {
        if (err) {
          res.send('Error, the game was not deleted. Go <a href="/score/list">back</a>.');
        } else {
          // Delete results from bd
          database.collection('result').remove({
            gameId: game
          }, function(err) {
            if (err) {
              res.send('Error, the game results was not deleted. Go <a href="/score/list">back</a>.');
            } else {
              res.send('The game was deleted. Go <a href="/score/list">back</a>.');
            }
          });
        }
      });
    } else {
      res.send('404');
    }
  } catch (e) {
    console.log('Error, page not rendered.');
    console.log(e);
  }
};